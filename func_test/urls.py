from django.urls import path
from .views import home, confirmation

app_name = 'func_test'

urlpatterns = [
    path('', home, name='homepage'),
    path('confirm/', confirmation, name='confirm'),
]
