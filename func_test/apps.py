from django.apps import AppConfig


class FuncTestConfig(AppConfig):
    name = 'func_test'
