from django.shortcuts import render, redirect,reverse
from .forms import DataForm
from .models import Data
from django.views.generic import UpdateView
# Create your views here.


form_data = [{}]

def home(request):
    if request.method == "POST":
        form = DataForm(request.POST)
        name = request.POST.get("name")
        status = request.POST.get("status")
        form_data[0]['name'] = name
        form_data[0]['status'] = status
        if form.is_valid():
            return redirect("/confirm/")
        else:
            return redirect("/")
    else:
        return render (request, 'func_test/page.html',context={'datas' : Data.objects.all()} )

def confirmation(request):
    if request.method == "POST":
        form = DataForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/")
    else :
        return render(request, 'func_test/confirmation.html', {'form_data':form_data})

    