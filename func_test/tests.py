from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
import unittest 
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from .views import home, confirmation
from .models import Data

# Create your tests here.


class UnitTest(TestCase):
    def test_home_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_confirmation_url_exist(self):
        response = Client().get('/confirm/')
        self.assertEqual(response.status_code, 200)

    def test_home_view_exists(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_confirmation_view_exists(self):
        found = resolve('/confirm/')
        self.assertEqual(found.func, confirmation)

    def test_form_works(self):
        Data.objects.create(name="Linguini", status="Little Chef")

        num = Data.objects.all().count()
        self.assertEqual(num,1)

    def test_home_page_is_written(self):
        self.assertIsNotNone('/templates/func_test/page.html')

    def test_confirmation_page_is_written(self):
        self.assertIsNotNone('/templates/func_test/confirmation.html')

        
        


class NewDataTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.quit()

    def test_can_submit_and_display(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(30)

        # wait = WebDriverWait(self.browser, 40)
        name = self.browser.find_element_by_id('name')
        status = self.browser.find_element_by_id('status')
        # name = wait.until(EC.presence_of_element_located((By.NAME, 'name')))
        # status = wait.until(EC.presence_of_element_located((By.NAME, 'status')))

        # Homepage
        name.send_keys("SAMAL")
        status.send_keys("I'm Gay")
        name.submit()

        # Confirmation Page
        c_name = self.browser.find_element_by_id('name')
        c_name.submit()

        time.sleep(10)
        self.assertIn("Samal", self.browser.page_source)
        self.assertIn("I'm Gay", self.browser.page_source)

if __name__ == 'main':
    unittest.main(warnings='ignore')


