from selenium import webdriver
import time

driver = webdriver.Chrome()
driver.get('http://127.0.0.1:8000/')
time.sleep(5)

name = driver.find_element_by_id('name')
status = driver.find_element_by_id('status')

# Homepage
name.send_keys("JO")
status.send_keys("ALIN")
name.submit()

# Confirmation Page
c_name = driver.find_element_by_id('name')
c_name.submit()

assert "JO" in driver.page_source
assert "ALIN" in driver.page_source

time.sleep(5)
driver.quit()